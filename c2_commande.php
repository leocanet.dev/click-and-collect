<?php
include 'm_data_func.php';


function CoordonneesClient($id_cmd) {
    $id_clients = selectValuesWhere('Commande', 'id_client', 'id', $id_cmd);
    $id_client = $id_clients[0];
    $CoordClients = selectDataWhere('CoordonneesClient', 'id', $id_client);
    $CoordClient = $CoordClients[0];
    return $CoordClient;
}

function mail_CMD_Prepa($emailClient,$nCMD) {

    $sujet = "Commande N°".$nCMD;
    // Le message
    $message = "Bonjour ! votre commande N°".$nCMD." est prête !\r\n \r\nVous pouvez venir la récupérer, a nos horraires d'ouverture\r\n \r\nCordialement l'equipe de LA FERME";

     // Dans le cas où nos lignes comportent plus de 70 caractères, nous les coupons en utilisant wordwrap()
    $message = wordwrap($message, 70, "\r\n");

    // Envoi du mail
    mail($emailClient, $sujet, $message);
  

}

function mail_CMD_Collect($emailClient,$nCMD) {

    $sujet = "Commande N°".$nCMD;
   // Le message
    $message = "Merci de votre visite !\r\n \r\nNous espérons vous revoir très bientot !\r\n \r\nCordialement l'equipe de LA FERME";

    // Dans le cas où nos lignes comportent plus de 70 caractères, nous les coupons en utilisant wordwrap()
    $message = wordwrap($message, 70, "\r\n");

    // Envoi du mail
    mail($emailClient, $sujet, $message);

}


if (isset($_POST["btnPrepa"]) ) {

$mailPrepa = $_POST["mailPrepa"];
$idPrepa = $_POST["idPrepa"];

mail_CMD_Prepa($mailPrepa , $idPrepa );
etatChange("prête", $idPrepa);


header('location: v2_commande.php');
}


if (isset($_POST["btnCollect"]) ) {

$mailCollect = $_POST["mailCollect"];
$idCollect = $_POST["idCollect"];

mail_CMD_Collect($mailCollect , $idCollect );
etatChange("collectée", $idCollect);

header('location: v2_commande.php');
}


?>