<?php

include 'm_data_func.php';

if(
    isset($_POST['panier']) &&
    isset($_POST['total']) &&
    isset($_POST['nomClient']) &&
    isset($_POST['email']) &&
    isset($_POST['tel']) &&
    $_POST['panier'] !== "" &&
    $_POST['total'] !== "" &&
    $_POST['nomClient'] !== "" &&
    $_POST['email'] !== "" &&
    $_POST['tel'] !== ""
){
    $panier = $_POST['panier'];
    $panier = json_decode($panier, true);
    $totalPanier = $_POST['total'];
    $nomClient = $_POST['nomClient'];
    $mailClient = $_POST['email'];
    $telClient = $_POST['tel']; 
    
    $id_client = insertData("CoordonneesClient", ["nom","email","telephone"],[$nomClient,$mailClient,$telClient]);
    $id_cmd = insertData("Commande", ["prix_total", "etat", "id_client"], [$totalPanier, "validée", $id_client]);
    foreach($panier as $elem => $key) {
        
    insertData("CommandeProduit", ["id_cmd", "id_prod", "quantite"],[$id_cmd, $key['idProd'], $key['quantite']]);
    };
}

// ___________________________________________________________________________________________

var_dump($panier);


// ___________________________________________________________________________________________

header('location: index.php');

?>