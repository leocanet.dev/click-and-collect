<!DOCTYPE html>
<html lang="fr">

<!--  include './m_data_func.php';?> -->
<?php include 'c2_produit.php';
        session_start();
        if(!$_SESSION['password']){
        header('Location: index.php');}
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Modification des produits et des infos de la ferme">
    <link rel="stylesheet" href="./css/styleAdmin.css">

    <title>Gestion Stock</title>
</head>

<body>

    <?php include 'v0_header_admin.php'; ?>

    <main>

        <div class="container2">


            <!-- form AJOUTER / MODIFIER  ANNULER =========================== -->
            <h2> PRODUITS: Ajouter / Modifier / Supprimer </h2>
            <div class="border-2b padding-V padding-H">
                <!-- Récupérer produit_select si un produit à été sélectionner par btn submit VoirModif -->
                <?php
                    $action = 'AJOUTER';
                    if(isset($produit_select)){
                        $action = 'MODIFIER';
                }?>
                <!-- Afficher le formulaire d' $action = AJOUTET / MODIFIER (ou ANNULER) -->
                <form class="ligne" enctype="multipart/form-data" action="c2_produit.php" method="post">
                    <!-- Demander info produit (ou afficher produit_select if isset -->
                    <div class="col flex4">
                        <div class="ligne axe1-start">
                            <label for="nom"> Produit: </label>
                            <input type="text" name="nom"
                                value="<?= isset($produit_select)? $produit_select['nom'] : ""; ?>">
                        </div>
                        <div class="ligne axe1-start">
                            <label for="imgFile"> Image: </label>
                            <input type="file" name="imgFile" accept="image/png, image/jpeg, image/jpg,"
                                value="<?= isset($produit_select)? $produit_select['imgFile'] : ""; ?>">
                        </div>
                    </div>
                    <div class="col flex4">
                        <div class="ligne axe1-start">
                            <label for="prixAuKg"> Prix (-.-- €/Kg): </label>
                            <input type="number" step="0.01" name="prixAuKg"
                                value="<?= isset($produit_select)? $produit_select['prixAuKg'] : ""; ?>">
                        </div>
                        <div class="ligne axe1-start">
                            <label for="stock"> Stock (-.--- Kg): </label>
                            <input type="number" step="0.001" name="stock"
                                value="<?= isset($produit_select)? $produit_select['stock'] : ""; ?>">
                        </div>
                    </div>
                    <div class="ligne flex2">
                        <label for="dispo"> Dispo: </label>
                        <input type="checkbox" name="dispo"
                            value="<?= isset($produit_select)? $produit_select['dispo'] : ""; ?>">
                    </div>
                    <!-- Saisir id et action -->
                    <input type="hidden" name="id" value="<?= isset($produit_select)? $produit_select['id'] : ""; ?>">
                    <input class="btn flex1 col axe1-center axe2-center margin-H" type="submit" name="<?= $action; ?>"
                        value="<?= $action; ?>">
                    <a class="btn flex1 col axe1-center axe2-center margin-H" href="v2_produit.php">ANNULER</a>
                </form>
            </div>

            <!-- AFFICHER Boite scroll PRODUITS  -->

            <div class="stockBox">
                <?php $tab_produits = selectAllData('Produit'); ?>
                <?php foreach($tab_produits as $produit_i) { ?>
                <div class="ligne axe2-center">
                    <!-- Afficher Produit: info -->
                    <img class="flex1 imgContaint imgSize70h80w"
                        src="<?php  echo "./images/produits/".$produit_i[ "imgFile" ] ?>" alt="IMG">

                    <h4 class="flex1"> <?php  echo $produit_i[ "nom" ] ?> </h4>
                    <h4 class="flex1"> <?php  echo $produit_i[ "prixAuKg" ] ?> €/Kg </h4>
                    <h4 class="flex1"> <?php  if ($produit_i[ "dispo" ]=='1') {echo "Dispo";} else {echo"Indispo";} ?>
                    </h4>
                    <h4 class="flex1"> Stock: <?php  echo $produit_i[ "stock" ] ?> Kg </h4>

                    <!-- Afficher Produit: action VOIR-pour-MODIFF / SUPPR -->
                    <form class="flex2 ligne" action="c2_produit.php" method="post">
                        <a class="btn flex1 col axe1-center axe2-center margin-H"
                            href="v2_produit.php?id=<?= $produit_i['id']; ?>">MODIFIER</a>

                        <input type="hidden" name="id" value="<?= $produit_i['id']; ?>">
                        <input class="btn flex1 col axe1-center axe2-center margin-H" type="submit" name="SUPPRIMER"
                            value="SUPPRIMER">
                    </form>

                </div> <!-- Fin div ligne produit_i "prod_Fiche"-->
                <?php  } ?>
            </div> <!-- Fin div tab_produits "stockBox" -->

            <!-- form INFORMATION : Modifier  ====================================== -->
            <h2> Information </h2>
            <div class="border-2b padding-V padding-H">

                <?php $info = selectData('Information',1); ?>
                <form class="ligne axe2-center" action="c2_produit.php" method="post">
                    <!-- Demander info produit (ou afficher produit_select if isset -->
                    <div class="col flex2 axe1-start margin-H">
                        <label for="adresse"> Adresse: </label>
                        <textarea name="adresse" rows="3" cols="33"><?php echo $info['adresse'] ?>
                    </textarea>
                    </div>

                    <div class="col flex2 axe1-start margin-H">
                        <label for="horaire"> Horaire: </label>
                        <textarea name="horaire" rows="3" cols="33"><?php echo $info['horaire'] ?>
                    </textarea>
                    </div>

                    <div class="col flex2 axe1-start margin-H">
                        <label for="telephone"> Tel: </label>
                        <input type="text" name="telephone" value="<?php echo $info['telephone'] ?>">
                    </div>

                    <input type="hidden" name="id" value="<?php echo $info['id'] ?>">
                    <input class="btn flex1 col axe1-center axe2-center margin-H" type="submit" name="MODIF_INFO"
                        value="MODIFIER">
                    <a class="btn flex1 col axe1-center axe2-center margin-H" href="v2_produit.php">ANNULER</a>
                </form>
            </div>

        </div> <!-- Fin div "container2" global -->

    </main>

    <?php include 'v0_footer.php';  ?>

</body>

</html>