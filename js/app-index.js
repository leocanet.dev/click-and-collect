let panierHTML = document.getElementById("produit-area");
let inputQte = document.getElementsByClassName("inputqt");
let prodQt = document.getElementsByClassName("prod-qt");
let priceProduit = document.getElementsByClassName("price-produit");
let contenuPanier = document.getElementById("contenue");
let panier = {};
// Ajout des produits dans panier[produits] -> nom : quantité
for (const input of inputQte) {
  input.addEventListener("change", (event) => {
    produit = event.target.getAttribute("data-name");
    prix = event.target.getAttribute("data-price");
    img = event.target.getAttribute("data-img");
    idProd = event.target.getAttribute("data-id-prod");
    quantite = event.target.value;
    panier[produit] = {
      nom: produit,
      prix: prix,
      image: img,
      quantite: quantite,
      idProd: idProd,
    };
    contenuPanier.value = JSON.stringify(panier);

    majHTML(panier);
    majTotalPrix();
  });
}

function majHTML(panier) {
  // Rafraichis l'affichage pour éviter les doublons
  panierHTML.innerHTML = "";
  for (ligne in panier) {
    var prod = panier[ligne];
    //_____________Generation des prods dans le panierHTML________________________________________

    panierHTML.innerHTML += ` <div class=" box-list">
    <div class="box-img">
        <img src="${prod.image}" class="img-prod-panier">
    </div>
    
    <div class="box-qte">
       <p class="prod-qt">${prod.quantite}g</p>
       <i class="fas fa-minus-square btn-suppr" onclick="removeProduct(id)"></i> 
    </div>
    
    <div class="box-name">
        <div class="name-panier">
            <p class="name-produit">${prod.nom}</p>
        </div>
    </div>
    
    <div class="box-price">
        <p class="price-produit">${((prod.prix * prod.quantite) / 1000).toFixed(
          2
        )} €/Kg</p>
    </div>
    </div> `;
    //____________________________________________________________________________________________
  }
  // Ajout dans le localStorage
  localStorage.setItem("panier", JSON.stringify(panier));
}
// Maj du total panier
function majTotalPrix() {
  let total = document.getElementById("total-panier");
  let inputTotal = document.getElementById("input-total-panier");
  let somme = 0;
  for (const input of Object.values(panier)) {
    let prix = input.prix;
    let val = input.quantite;
    somme += (prix * val).toFixed(2) / 1000;
  }
  total.innerText = "Total : " + somme.toFixed(2) + "  €";
  inputTotal.value = somme.toFixed(2);
  total.style.fontStyle = "italic";
}

// suppr produit panier |OFF|
let btnSuppr = document.getElementsByClassName("btn-suppr");
function removeProduct(id) {}

// AddEvent on Scroll Panier
const cart = document.getElementById("panier");
const list = document.getElementById("liste-produits");
window.addEventListener("scroll", function () {
  scrollValue =
    ((window.innerHeight + window.scrollY) / document.body.offsetHeight) * 100;
  // console.log(scrollValue);

  // Panier et liste produits apparaissent en fonction de la scollValue
  if (scrollValue > 79) {
    cart.style.opacity = "1";
    cart.style.transform = "none";
    list.style.opacity = "1";
    list.style.transform = "none";
  }
});

// Si le produit est indispo ajout de la classe indispo et impossibilité de l'ajouter au panier
let boxPro = document.getElementsByClassName("box-product");
for (box of boxPro) {
  let dispo = box.getAttribute("data-dispo");
  if (dispo == 1) {
    box.classList.add("prod-dispo");
  } else {
    box.classList.add("prod-indispo");
  }
}
