<!DOCTYPE html>
<html lang="fr">

<?php
    include 'c2_commande.php';
?>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Gestion des commandes des utilisateurs">
    <link rel="stylesheet" href="./css/styleAdmin.css" />
    <title>Gestion Commande</title>
</head>


<body>



    <?php include 'v0_header_admin.php'; 
    
    session_start();
    if(!$_SESSION['password']){
    header('Location: index.php');}
    ?>

    <main>
        <div class="container1" id="container1">
            <div class="boxleft">
                <!-- ################################################################################################################### -->


                <!-- ------------------------------------------------------------------------------------------ -->
                <?php $Tab_commandeValidee = selectDataWhere('Commande',"etat","validée") ; ?>
                <?php foreach($Tab_commandeValidee as $e) { ?>

                <?php $coordonnees = CoordonneesClient($e["id"]) ; ?>
                <?php $emailClient = $coordonnees["email"] ; ?>

                <div class="ongletCMD">
                    <div class="commande" id="N°<?php echo $e["id"] ?>">
                        <h3>Commande N°<?php echo $e["id"] ?></h3>
                        <p> en attente de préparation</p>
                    </div>
                    <form action="c2_commande.php" method="post">
                        <input type="hidden" name="idPrepa" value="<?php echo $e["id"] ?>">
                        <input type="hidden" name="mailPrepa" value="<?php echo $emailClient ?>">
                        <input class="btn" type="submit" name="btnPrepa" value="PRÊTE  ✔">
                    </form>
                </div>

                <div class="clientCMD" id="fiche_N°<?php echo $e["id"] ?>">

                    <?php $commande = ProduitQtCmd($e["id"]) ; ?>
                    <?php foreach($commande as $contenue) { ?>

                    <div class="prod">
                        <p><?php echo $contenue["nom"] ?></p>
                        <p><?php echo $contenue["quantite"] ?></p>
                        <p><?php echo $contenue["stock"] ?></p>
                    </div>

                    <?php }?>

                </div>

                <div class="clientINFO" id="info_N°<?php echo $e["id"] ?>">
                    <h4><?php echo $coordonnees["nom"] ?></h4>
                    <h4><?php echo $coordonnees["email"] ?></h4>
                    <h4><?php echo $coordonnees["telephone"] ?></h4>
                </div>

                <?php }?>

                <!-- ------------------------------------------------------------------------------------------ -->
                <?php $Tab_commandePrete = selectDataWhere('Commande',"etat","prête") ; ?>
                <?php foreach($Tab_commandePrete  as $e) { ?>

                <?php $coordonnees = CoordonneesClient($e["id"]) ; ?>
                <?php $emailClient = $coordonnees["email"] ; ?>

                <div class="ongletCMD">
                    <div class="commande pret" id="N°<?php echo $e["id"] ?>">
                        <h3>Commande N°<?php echo $e["id"] ?></h3>
                        <p> en attente de retrait </p>
                    </div>
                    <form action="c2_commande.php" method="post">
                        <input type="hidden" name="idCollect" value="<?php echo $e["id"] ?>">
                        <input type="hidden" name="mailCollect" value="<?php echo $emailClient ?>">
                        <input class="btn" type="submit" name="btnCollect" value="SUPPR ✗">
                    </form>
                </div>

                <div class="clientCMD" id="fiche_N°<?php echo $e["id"] ?>">

                    <?php $commande = ProduitQtCmd($e["id"]) ; ?>
                    <?php foreach($commande as $contenue) { ?>

                    <div class="prod">
                        <p><?php echo $contenue["nom"] ?></p>
                        <p><?php echo $contenue["quantite"] ?></p>
                        <p><?php echo $contenue["stock"] ?></p>
                    </div>

                    <?php }?>

                </div>

                <div class="clientINFO" id="info_N°<?php echo $e["id"] ?>">
                    <h4><?php echo $coordonnees["nom"] ?></h4>
                    <h4><?php echo $coordonnees["email"] ?></h4>
                    <h4><?php echo $coordonnees["telephone"] ?></h4>
                </div>

                <?php }?>
                <!-- ------------------------------------------------------------------------------------------ -->

            </div>
            <!-- ################################################################################################################################## -->

            <div class="boxright">
                <h3 id="numCMD">N°Commande</h3><!--  <<<INNERHTML   -->
                <div class="table">
                    <h4>Nom</h4>
                    <h4>Quantité</h4>
                    <h4>Stock</h4>
                </div>

                <div class="contenuCMD" id="fiche">
                    <!--  CLONE  -->
                </div>
            </div>
        </div>
    </main>

    <?php include 'v0_footer.php';  ?>

    <script src="./js/scriptAdmin.js"></script>
</body>

</html>

</html>