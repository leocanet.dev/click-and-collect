<!DOCTYPE html>
<html lang="fr">
<?php
      include 'm_data_func.php';
      session_start();
      $_SESSION = array();
      session_destroy();
?>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Présentation de notre ferme ainsi que ses produits">
    <link rel="stylesheet" href="./css/styles-index.css">
    <title>Click & Collect</title>
</head>

<body>
    <header>
        <div class="box-header">
            <div id="box-txtheader">
                <p>Ferme Simplon</p>
            </div>
        </div>
    </header>
    <div class="wrapper">
        <div class="p-wrap">
            <?php $tab_Information = selectAllData('Information'); ?>
            <p class="txt-header"> <span>➽</span> <?php  echo $tab_Information[0]['adresse']?></p>
            <p class="txt-header"><?php  echo $tab_Information[0]['horaire']?></p>
            <p class="txt-header"> ☎ <?php  echo $tab_Information[0]['telephone']?></p>
        </div>
        <img src="./images/Cart.svg" alt="fermier" id="fermier-svg">
    </div>
    <main>
        <div class="boxfusion">
            <!-- BOUCLE FOR PRODUCTS-->
            <div class="list" id="liste-produits">
                <?php $produit = selectAllData('Produit'); ?>
                <?php foreach($produit as $i) { ?>
                <div class=" box-product" id="<?php  echo $i["id"] ?>" data-dispo="<?php  echo $i["dispo"] ?>">
                    <p class="name-product"><?php  echo $i["nom"] ?></p>
                    <!-- <img src="./images/potatoes.jpg" width="100%" height="100%" alt="product"> -->
                    <img class="imgSize140h100w imgContaint" src="<?php  echo "./images/produits/".$i["imgFile"] ?>"
                        alt="IMG">
                    <div class="priceqte">
                        <p class="price"><?php  echo $i["prixAuKg"] ?> €/Kg</p>
                        <input type="number" class="inputqt" id="<?php  echo $i["nom"] ?>" step="100" min="100"
                            max="6000" value="100" data-price="<?php  echo $i["prixAuKg"] ?>"
                            data-name="<?php  echo $i["nom"] ?>"
                            data-img="<?php  echo "./images/produits/".$i["imgFile"] ?>"
                            data-id-prod="<?php  echo $i["id"] ?>">
                    </div>
                </div>
                <?php }?>
            </div>
            <!-- SECTION PANIER -->
            <div id="panier">
                <div class="box-panier">
                    <p id="text-panier">🛒 Simplon</p>
                </div>
                <!-- PRODUIT DANSLE PANIER -->
                <div class="panier-area" id="produit-area">
                    <!-- Les produits sont générés dynamiquement en JS ici -->
                </div>
                <form id="form-panier" action=" panier.php" method="post">
                    <div class="box-validate">
                        <div id="box-total">
                            <input id="input-total-panier" type="hidden" value="" name="total">
                            <span id="total-panier"> 📠 Total :</span>
                        </div>
                        <input id="contenue" type="hidden" name="panier">
                        <input class="inputInfo" type="text" name="nomClient" placeholder=" ➣ Votre nom..."
                            id="nomClient" required>
                        <input class="inputInfo" type="text" name="email" placeholder=" ✉ Adresse mail..." id="mail"
                            required>
                        <input class="inputInfo" type="text" name="tel" placeholder=" ✆ n° tel..." id="tel" required>
                        <button type="submit" id="validate">Valider</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
    <footer>
        <a href="login.php" class="a-login">©</a>
        <p>
            Copyright 2022 - Simplon
        </p>
    </footer>
    <script src="https://kit.fontawesome.com/79730b03eb.js" crossorigin="anonymous"></script>
    <script src="./js/app-index.js"></script>
</body>

</html>