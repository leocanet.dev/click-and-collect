
-- Use script: sudo mysql < 0_DB_init.sql;

-- -------------------------------------------------------------
-- DATABASE:    ClickAndCollect;
-- TYPE ENUM:   EtatCommande
-- TABLE:       Commande
--              Produit
--              LigneCommande
--              Information
--              CoordonneesClient
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use ClickAndCollect;
-- show tables;
-- describe Commande;
-- select * from Commande;
-- quit;
-- exit;
-- -------------------------------------------------------------

DROP DATABASE IF EXISTS ClickAndCollect;

CREATE DATABASE ClickAndCollect;

GRANT ALL PRIVILEGES ON ClickAndCollect.* TO 'admin2'@'localhost';

USE ClickAndCollect;


-- NEW TABLE -----------------------------
-- 
CREATE TABLE CoordonneesClient (
    id          INT unsigned auto_increment PRIMARY KEY,
    nom         varchar(50),
    email       varchar(75),
    telephone  varchar(10)
);

CREATE TABLE Commande (
    id          INT unsigned auto_increment PRIMARY KEY,
    etat        ENUM ('panier', 'validée', 'prête', 'collectée'),
    id_client   INT unsigned,
    prix_total  FLOAT DEFAULT 0.0,      
    UNIQUE(id, id_client),      
    FOREIGN KEY (id_client) REFERENCES CoordonneesClient(id)
);
-- -- Commande et CoordonneesClient sont lies
-- -- Mettre id_client dans Commande... surement ou l inverse...

CREATE TABLE Produit (
    id          INT unsigned auto_increment PRIMARY KEY,
    nom         varchar(50),
    imgFile     varchar(200),
    prixAuKg    FLOAT,
    dispo       BOOLEAN,
    stock       FLOAT DEFAULT 10.0
);

CREATE TABLE CommandeProduit (
    id_cmd      INT unsigned,
    id_prod     INT unsigned,
    quantite    FLOAT,
    UNIQUE(id_cmd, id_prod),      
    FOREIGN KEY (id_cmd) REFERENCES Commande(id),
    FOREIGN KEY (id_prod) REFERENCES Produit(id)
);

CREATE TABLE Information (
    id          INT unsigned auto_increment PRIMARY KEY,
    adresse     varchar(200),
    telephone  varchar(16),
    horaire     varchar(350)
);

CREATE TABLE Administrator (
    id          INT unsigned auto_increment PRIMARY KEY,
    pseudo      varchar(20),
    password    varchar(20)  
)

-- id pas necessaire:
-- 1 seule ligne de données avec les informations du ma Ferme
-- Etape futur: ClickAndCollect chez differents Producteurs




