
-- Use script: sudo mysql < 0_DB_fill.sql;

-- DATABASE:    ClickAndCollect;
-- TYPE ENUM:   EtatCommande
-- TABLE:       Commande
--              Produit
--              LigneCommande
--              Information
--              CoordonneesClient
-- -------------------------------------------------------------
-- unix> sudo mysql
-- mysql> ...
-- show databases;
-- use ClickAndCollect;
-- show tables;
-- describe Commande;
-- select * from Commande;
-- quit;
-- exit;
-- -------------------------------------------------------------

USE ClickAndCollect;

-- TABLE Produit  -----------------------------

INSERT INTO Produit (nom, imgFile, prixAuKg, dispo)
    VALUES 
            ( "Aubergines", "aubergines.jpg", 2.35, true),
            ( "Avocats", "avocats.jpg", 2.60, true),

            ( "Carottes", "carottes.jpg", 1.60, true), 
            ( "Courgettes", "courgettes.jpg", 1.70, true),
            ( "Concombre", "concombres.jpg", 0.80, true),

            ( "Haricots verts", "haricots verts.jpg", 4.30, true),

            ( "Poivron vert", "poivron verts.jpg", 3.30, true),
            ( "Pommes de terre", "patates.jpg", 1.10, true),

            ( "Tomate grappe", "tomate_grappe.jpg", 2.00, true),
            ( "Tomate cerise", "tomate_cerise.jpg", 5.00, true),

            ( "Salade laitue", "salade laitue.jpg", 1.40, true),
            ( "Salade batavia", "salade batavia.jpg", 2.40, true),
            ( "Salade mache", "salade mache.jpg", 5.60, true);


INSERT INTO CoordonneesClient (nom, email, telephone)
    VALUES 
            ( "Cyril GALERA", "cyr.galera@gmail.com", "0603662741"),
            ( "Arnaud BOUTILLER", "aeboutillier@gmail.com","0643146258"),
            ( "Leo CANET", "leocanet.dev@gmail.com","0643146258"),
            ( "Mr TEST", "yinavac262@douwx.com","0600000000");
          


INSERT INTO Commande (etat, id_client)
    VALUES 
            ('collectée', 1),
            ('prête', 3),
            ('prête', 1),
            ('prête', 2),
            ('validée', 4),
            ('validée', 4),
            ('validée', 4),
            ('validée', 4),
            ('validée', 4);
            


INSERT INTO Commande (etat)
    VALUES 
            ('panier'),
            ('panier'),
            ('panier');


INSERT INTO CommandeProduit (id_cmd, id_prod, quantite)
    VALUES 
            (1, 1, 1.1),
            (1, 2, 1.2),
            (2, 1, 2.1),
            (2, 3, 2.3),
            (2, 5, 2.5),
            (3, 1, 3.1),
            (3, 3, 3.3),
            (3, 5, 3.5),
            (4, 4, 4.4),
            (5, 5, 5.5),
            (6, 6, 6.6),
            (7, 1, 2.1),
            (8, 3, 2.3),
            (9, 1, 2.1),
            (10, 3, 2.3);


INSERT INTO Information (adresse, telephone, horaire)
    VALUES 
            ("Av de Toulouse / 11100 Narbonne",
             "0601234567",
             "Lundi au Vendredi: 9h-12h30 14h-18h / Samedi: 9h-12h30");


INSERT INTO Administrator (pseudo, password)
    VALUES 
            ("bigbob", "toto123");











