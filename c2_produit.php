<?php
    include 'm_data_func.php';

// Return   - a Array of values of all $_POST['inputNames_i'] 
// Return   - NULL if prob (Check isset and if value !="")
//
function getPOST($inputNames) {
        $inputValues = [];

        foreach($inputNames as $name_i) {
            if( isset($_POST[$name_i]) && $_POST[$name_i] !== "") {
                array_push($inputValues, $_POST[$name_i]);
            } else {
                return NULL;
            }
        }
        return $inputValues;
}
// Return   - the(one) value of FILES[$inputName]
//              also upload the file in $pathDirName (ex: "./images") 
// Return   - NULL if prob (Check isset and if value !="")
//
function getUploadFILES($inputName,$pathDirName) {
    
    // get $_FILES
    if( isset($_FILES[$inputName]) && $_FILES[$inputName]['name'] !== "") {
        $inputValue = $_FILES[$inputName];
    } else {
        return NULL;
    }
    
    // upload $_FILES
    $nomFichier = $_FILES['imgFile']['name'];
    // $toPathFile = 
    if (move_uploaded_file($_FILES[$inputName]['tmp_name'], $pathDirName.$nomFichier)) {
        print "Téléchargé avec succès!";
    } else {
        print "Échec du téléchargement!";
    }
    //
    return $nomFichier;
}

// CONTROL

// CRUD =========================================================
// Create 
// Read
// Update
// Delete

// CRUD =========================================================
// Create 
if (isset($_POST['AJOUTER'])){
    echo "</p> Produit_Ajout";
    // Check and Get all $_POST['name_i_TableDBB']
    $inputNames = [ 'nom', 'prixAuKg', 'stock' ];    
    $inputValues = getPOST($inputNames);
    if( $inputValues === NULL ) { return NULL; }
    
    // Get Boolean 
    $dispo  = (isset($_POST['dispo']))? 1: 0;
    array_push($inputNames, 'dispo');
    array_push($inputValues, $dispo);

    // Get FILES 'imgFile'
    $inputFileVal = getUploadFILES('imgFile','./images/produits/');
    if( $inputFileVal === NULL ) { return NULL; }
    array_push($inputNames, 'imgFile');
    array_push($inputValues, $inputFileVal);
      
    // 
    insertData('Produit', $inputNames, $inputValues);
    header('location: v2_produit.php');
}

// CRUD =========================================================
// Read
//
if (isset($_GET['id'])){
    echo "recuperation d'un produit";
    $produit_select = selectData('Produit',$_GET['id']);
}

// CRUD =========================================================
// Update
//
if (isset($_POST['MODIF_INFO'])){
    echo "</p> INFO: modifier";
    // Get values of all $_POST['...']
    $inputNames = [ 'adresse', 'telephone', 'horaire' ];    
    $inputValues = getPOST($inputNames);
    if( $inputValues === NULL ) { return NULL; }
    
    $info_updated = updateData('Information', $inputNames, $inputValues, $_POST['id']);
   
    echo "</p></p>"."Info: bien modifié";
    header('location: ./v2_produit.php');
}

if (isset($_POST['MODIFIER'])){
    echo "</p> Produit: modifier";
    // Get values of all $_POST['...']
    $inputNames = [ 'nom', 'prixAuKg', 'stock' ];    
    $inputValues = getPOST($inputNames);
    if( $inputValues === NULL ) { return NULL; }
    
    // Get POST Boolean 
    $dispo  = (isset($_POST['dispo']))? 1: 0;
    array_push($inputNames, 'dispo');
    array_push($inputValues, $dispo);

    // Get FILES 'imgFile' if any modif
    $inputFileVal = getUploadFILES('imgFile','./images/produits/');
    if( $inputFileVal !== NULL ) { 
        array_push($inputNames, 'imgFile');
        array_push($inputValues, $inputFileVal);
    }

    // Check param inputNames inputValues
    // 
    $produit_select = updateData('Produit', $inputNames, $inputValues, $_POST['id']);
    echo "</p></p>"."Produit: bien modifié";
    header('location: ./v2_produit.php');
}

// CRUD =========================================================
// Delete
if (isset($_POST['SUPPRIMER'])){
    echo "suppr d'un produit";
    deleteData('Produit',$_POST['id']);
    echo "supprimé";
    header('location: ./v2_produit.php');
}

?>