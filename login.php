<?php
// Demarre une session pour un utilisateur
session_start();

include 'm_data_func.php';

//Lorsque l'utilsateur clique sur le bouton d'envoi, je déclare mes variables  
if(isset($_POST['envoi'])){
    global $pdo;
    $pseudo = $_POST['pseudo'];
    $mdp = $_POST['pwd'];
// Je recupere sous forme de tableau,le mot de passe et le pseudo dans la table admin de ma base de donnée
    $recup_user = $pdo->prepare('SELECT * FROM Administrator WHERE pseudo = ? AND password = ?');
    $recup_user->execute(array($pseudo, $mdp));
// Je verifie si les informations sont présentes dans la table
    if($recup_user->rowCount() > 0){
        global $pdo;
        $_SESSION['pseudo'] = $pseudo;
        $_SESSION['password'] = $mdp;
        $_SESSION['id'] = $recup_user->fetch(['id']);
// Si les identifiants sont correct : l'utilisateur est renvoyé vers add.php
        header('Location: v2_commande.php');
    }else{
// Si les identifiants sont incorrect : l'utilisateur est renvoyé vers index.html via la balise meta HTML   
        $bye = "Vos identifiants sont incorrect...";
    }
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/styleAdmin.css" />
    <title>Login</title>
</head>

<body>
    <div class="loginform">
        <form action="login.php" method="post" class="form-login">
            <label for="pseudo">ID</label>
            <input class="logtext" type="text" name="pseudo" placeholder="Entre ton pseudo..">
            <label for="pwd">PASSWORD</label>
            <input type="password" name="pwd" placeholder="Entre ton mot de passe..">
            <input class="log" type="submit" id="logbtn" name="envoi" value="Login">
        </form>
    </div>
</body>

</html>